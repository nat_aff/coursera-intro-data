---
title: "Probability and Data -- Week 2"
output: html_document
---

# Probability

_(Corresponds to Coursera videos for Week 3_ and 
_OS3 book Ch. 2) _

## Notation and definitions

* Probability (Frequentist contrasted with Bayesian)
* Set notations 
* Sample space 
* Event 

## Set algebra 

* Complement, union, intersection, negation 
* De Morgan's law
\[
  P(A \cup B)^c = P(A^c) \cap P(B^c)
\]

## Probability rules

* Disjoint or mutually exclusive
* Addition rule for disjoint outcomes 
\[
  P(A \cup B)  = P(A) + P(B)
\]

* General addition rule (Inclusion/exclusion)

## Preview of probability distributions

* Probability distribution (discrete or categorical) 
* Probability distributions rules (Kolmogorov axioms)
* Law of large numbers $
\[
  \hat p_n \to p \quad \text{ as } \quad n \to \infty
\]

## Independence 

* Independence rule 

If $A, B$ are independent events:
\[
  P(A|B) = P(A)
\]
and 
\[
 P(A \cap B) = P(A) \cdot P(B)
\]

* Multiplication rule for independent events

## Conditional probability 
* Conditional probabililty 
\[
  P(A|B) = \frac{P(A \cap B)}{P(B)}
\]

* Marginal and joint probability 
* Reading a contingency table 

## Bayes theorem 

* See exercises

Additional exercises: 

1. Define a small sample space $E$. _For example, $\{ h,t \}$ is a sample space of results of a coin flip._
2. Define 2 events, $(A, B)$ on the sample space. (Make sure $A \cap B \neq \emptyset)$.
3. Draw a Venn diagram representing: 

  * $P(A \cap B)$ 
  * $P(A \cup B)$
  * $P(A \cup B)^c$ 

4. Visually derive De Morgan's law using a Venn diagram:

  * $P(A \cup B)^c = P(A^c) \cap P(B^c)$ 

5. The inclusion-exclusion rule is: 
\[
  P(E \cup F) = P(E) + P(F) - P(E \cap F)
\]

a.) Given that: 
    
  * $P(E cup F) = .6$
  * $P(E) = .5$ 
  * $P(F) = .4$ 

 Find $P( E \cap F)$. 

b.) For the same $E, F$ what is $P(E |F)$. What does this look like in the venn diagram? 

c.) If you found $P(E |F)$ using algebra, translate the formula you used into an abstract algebra statement using the probability notation we've been using, ($P(E \cap F)$, $P(E)$, etc.)

6. Translate the probabilities in the previous example (or make up new numbers) into a contigency table. What should the event lables for the contigency table be? 

7. Give examples of joint and marginal probabilities from the contigency table. 

8. Given an example of events on some probability space that are independent and examples that are dependent.

9. Derive Bayes theorem using the conditional probability rule and the fact
\[
    P(A \cap B)  = P(B \cap A).
\]


